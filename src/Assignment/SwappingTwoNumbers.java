package Assignment;

public class SwappingTwoNumbers {

	public static void main(String[] args) {
		 int number1 = 10, number2 = 20;
		  System.out.println("before swapping number1 and number2 is "+number1+','+number2);
		  number1 = number1+number2;
		  number2 = number1-number2;
		  number1 = number1-number2;
		  System.out.println("after swapping number1 and number2 is "+number1+',' +number2);
		  


	}

}
