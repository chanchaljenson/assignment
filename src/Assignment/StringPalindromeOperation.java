package Assignment;

import java.util.Scanner;

public class StringPalindromeOperation {

	public static void main(String[] args) {
		String string, rev ="";
		System.out.println("enter a string ");
		Scanner sc=new Scanner(System.in);
		string= sc.nextLine();
		int length=string.length();
		for(int i=length-1;i>=0;i--) {
			rev=rev+string.charAt(i);
		}
		if(string.equalsIgnoreCase(rev)) {
			System.out.println("the given string is a palindrome " +string);
		} else
			System.out.println("the given string is not a palindrome " +string);
		}

	}

